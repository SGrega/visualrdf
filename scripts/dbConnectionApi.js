/* 
 * SPARQL - db connection - API functions
 * 
 * Created by GregaS.
 * */


$(document).ready(function() {
	//load classes into <option> list - SEARCH PAD
	dbCon_loadClassesList();
});


////////////////////////////////////////////////////////
////////////// VIEW PAD  ///////////////////////////////

//get data of subclasses (connections among classes) and convert them to arborJs format (to draw a graph - canvas)
function dbCon_showSubclasses_InArbor() {
	
	var qText = ['SELECT ?s ?p {  ', 
	' ?s rdfs:subClassOf ?p. ', 
	'} '].join('\n');

	//send request
	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			nodeInfo_lastJson = data;
			var data_tmp = {};

			$.each(data.results.bindings, function(i, tris) {
				var val= getStringFromURI(tris[data.head.vars[0]].value);
				var val2= getStringFromURI(tris[data.head.vars[1]].value);

				data_tmp[val + "->" + val2] = {
					from : "" + tris[data.head.vars[1]].value + "",
					property : "gsEmpty",
					to : "" + tris[data.head.vars[0]].value + ""
				};
			});

			//status = "clean" - clean old graph | "add" - append to existing graph
			general_pushSparqlDataToArbor(data_tmp, "clean");
		
		},	error : function(XMLHttpRequest, textStatus, errorThrown) {
			//$('#queryStatus').addClass('error').html("(getClassList)  XMLHttpRequest="+XMLHttpRequest.responseText+"\ntextStatus="+textStatus+"\nerrorThrown="+errorThrown);
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//end-ajax
}



///////////////////////////////////////////////////////////////////////
//////////// RIGHT SLIDER - EDITOR BOX  ///////////////////////////////
	

//// get full info of Graph Node/Search Entity - i.e. General,DatatypeProperty,FunctionalProperty,ObjectProperty,range,domain,subPropertyOf,...
function dbCon_nodeInfoFull(nodeUriId, node) {
	var qTxt="";

	var uriId = nodeUriId;
	selectedEntityInfo_uriId = nodeUriId;
	$('#infoOf').html("info of: <br>"+nodeUriId).addClass("editorDiv");
	
	//# send request - LABEL
	$('#editorBox_infoDiv-label').html("").removeClass('editorDiv'); //clean and hide
	var label = dbCon_getNode_specialPROPERTYvalue(uriId, 'rdfs:label');
	if (label.length > 0) {
		$('#editorBox_infoDiv-label').html("<b>label:</b> " + label).addClass('editorDiv');
	}

	//# send request - comment
	$('#editorBox_infoDiv-comment').html("").removeClass('editorDiv');
	var comment = dbCon_getNode_specialPROPERTYvalue(uriId, 'rdfs:comment');
	if (comment.length > 0) {
		$('#editorBox_infoDiv-comment').html("<b>comment:</b> " + comment).addClass('editorDiv');
	}

	//# send request - subClassOf
	$('#editorBox_infoDiv-subClassOf').html("").removeClass('editorDiv');
	var subClassOf = dbCon_getNode_specialPROPERTYvalue(uriId, 'rdfs:subClassOf');
	if (subClassOf.length > 0) {
		$('#editorBox_infoDiv-subClassOf').html("<b>subClassOf:</b> " + subClassOf).addClass('editorDiv');
	}

	//# send request - DatatypeProperty
	$('#editorBox_infoDiv-DatatypeProperty').html("").removeClass('editorDiv');
	qTxt = 'SELECT  DISTINCT ?prop   WHERE {  ?s a <' + nodeUriId + '>;    ?prop ?o.  ?prop a owl:DatatypeProperty .}';

	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qTxt) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			$.each(data.results.bindings, function(i, tris) {
				var val = (tris.prop.value) || ""; //you get full URI 
				var valFullURI = val;
				val = getStringFromURI(val); 
				
				var old = $('#editorBox_infoDiv-DatatypeProperty').html();
				if (old.length == 0) {
					old = "<b>DatatypeProperty</b>";
				}
				$('#editorBox_infoDiv-DatatypeProperty').html(old + '<br/>[<a href="#" onclick="addPropertyANDClassToFilterFromEntityInfo(&#39;' + val + '&#39;,this ,&#39;' + valFullURI + '&#39;)" >+</a>] ' + val).addClass('editorDiv');
			});
		}, 
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			//	 $('#editorBox_infoDiv-DatatypeProperty').html("DatatypeProperty:error");
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax

	//# send request - FunctionalProperty
	$('#editorBox_infoDiv-FunctionalProperty').html("").removeClass('editorDiv');
	qTxt = 'SELECT  DISTINCT ?prop   WHERE {  ?s a <' + nodeUriId + '>;    ?prop ?o.  ?prop a owl:FunctionalProperty .}';

	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qTxt) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			$.each(data.results.bindings, function(i, tris) {
				var val = (tris.prop.value) || "";//you get full URI 
				var valFullURI = val;
				val = getStringFromURI(val);
				
				var old = $('#editorBox_infoDiv-FunctionalProperty').html();
				if (old.length == 0) {
					old = "<b>FunctionalProperty</b>";
				}

				$('#editorBox_infoDiv-FunctionalProperty').html(old + '<br/>[<a href="#" onclick="addPropertyANDClassToFilterFromEntityInfo(&#39;' + val + '&#39;,this ,&#39;' + valFullURI + '&#39;)" >+</a>] ' + val).addClass('editorDiv');

			});
		}, 
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			// $('#editorBox_infoDiv-FunctionalProperty').html("FunctionalProperty:error");
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax

	//# send request - ObjectProperty
	$('#editorBox_infoDiv-ObjectProperty').html("").removeClass('editorDiv');
	qTxt = 'SELECT  DISTINCT ?prop   WHERE {  ?s a <' + nodeUriId + '>;    ?prop ?o.  ?prop a owl:ObjectProperty .}';

	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qTxt) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			$.each(data.results.bindings, function(i, tris) {
				var val = (tris.prop.value) || "";//you get full URI 
				var valFullURI = val;
				val = getStringFromURI(val);
				
				var old = $('#editorBox_infoDiv-ObjectProperty').html();
				if (old.length == 0) {
					old = "<b>ObjectProperty</b>";
				}
				$('#editorBox_infoDiv-ObjectProperty').html(old + '<br/>[<a href="#" onclick="addPropertyANDClassToFilterFromEntityInfo(&#39;' + val + '&#39;,this ,&#39;' + valFullURI + '&#39;)" >+</a>] ' + val).addClass('editorDiv');

			});
		}, 
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			//	 $('#editorBox_infoDiv-ObjectProperty').html("ObjectProperty:error");
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});
	//ajax

	///////////////////////
	///info o properties
	//| <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>    | <http://www.w3.org/2002/07/owl#FunctionalProperty>    |
	//| <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>    | <http://www.w3.org/2002/07/owl#DatatypeProperty>      |
	//| <http://www.w3.org/2000/01/rdf-schema#range>         | <http://www.w3.org/2001/XMLSchema#string>             |
	//| <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <http://www.lavbic.net/owl/AJPES.owl#naziv>           |
	//| <http://www.w3.org/2000/01/rdf-schema#domain>        | <http://www.lavbic.net/owl/AJPES.owl#PoslovniSubjekt> |
	//| <http://www.w3.org/2000/01/rdf-schema#label>         | "Polni naziv"@sl

	//# send request - range
	$('#editorBox_infoDiv-range').html("").removeClass('editorDiv');
	var range = dbCon_getNode_specialPROPERTYvalue(uriId, 'rdfs:range');
	if (range.length > 0) {
		$('#editorBox_infoDiv-range').html("<b>range</b>: " + range).addClass('editorDiv');
	}

	//# send request - domain
	$('#editorBox_infoDiv-domain').html("").removeClass('editorDiv');
	var domain = dbCon_getNode_specialPROPERTYvalue(uriId, 'rdfs:domain');
	if (domain.length > 0) {
		$('#editorBox_infoDiv-domain').html("<b>domain</b>: " + domain).addClass('editorDiv');
	}

	//# send request - subPropertyOf
	$('#editorBox_infoDiv-subPropertyOf').html("").removeClass('editorDiv');
	var subPropertyOf = dbCon_getNode_specialPROPERTYvalue(uriId, 'rdfs:subPropertyOf');
	if (subPropertyOf.length > 0) {
		$('#editorBox_infoDiv-subPropertyOf').html("<b>subPropertyOf</b>: " + subPropertyOf).addClass('editorDiv');
	}

	//# send request - GENERAL - all about this object
	$('#editorBox_infoDiv-GENERAL').html("").removeClass('editorDiv');
	qTxt = 'SELECT  ?prop ?s WHERE {<' + nodeUriId + '> ?prop ?s.}';

	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qTxt) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			$.each(data.results.bindings, function(i, tris) {
				var val = (tris.prop.value) || ""; 	//get full URI
				var val2 = (tris.s.value) || ""; 	//get full URI
				val = getStringFromURI(val);	
				val2 = getStringFromURI(val2);
				
				var old = $('#editorBox_infoDiv-GENERAL').html();
				if (old.length == 0) {
					old = "<b>GENERAL</b>";
				}
				$('#editorBox_infoDiv-GENERAL').html(old + "<br/><b>- " + val + ":</b> " + val2).addClass('editorDiv');
			});

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			//$('#editorBox_infoDiv-GENERAL').html("GENERAL:error");
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax
}

// get all info about selected node/entity on dbpedia(wikipedia)
function dbCon_nodeDBpediaInfo(uriId, node) {
	$('#editorBox_dbpediaInfoTitle').html("dbpedia info of: " + uriId).addClass("editorDiv");
	$('#editorBox_dbpediaInfoDiv').html("");

	//verify if propertyName d:dbpediaInfo exist on selected uriId
	var dbpediaResourceInfo = dbCon_getNode_specialPROPERTYvalue(uriId, 'd:dbpediaInfo');
	if (dbpediaResourceInfo.length == 0) {
		$('#editorBox_dbpediaInfoDiv').html("sorry selected entity does not have dbpediaInfo connection");
		return;	//end function -exit
	}

	$('#editorBox_dbpediaInfoTitle').html($('#editorBox_dbpediaInfoTitle').html() + "<br>dbpedia resource: " + dbpediaResourceInfo);

	var qTxt = [
	'SELECT ?prop ?s	', // you can change this to  DESCRIBE <uriId> 
	'WHERE {	', 
	'  SERVICE <http://dbpedia.org/sparql> {', 
	' <' + dbpediaResourceInfo + '> ?prop ?s', //e.g. <http://dbpedia.org/resource/University_of_Ljubljana>
	//' FILTER( lang(?s) = "en")', '  	', //Comment this line if you want all data from dbpedia
	'} }		', 
	'Limit 300',//140', 
	'		'].join('\n');

	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qTxt) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			nodeInfo_lastJson = data;
			$.each(data.results.bindings, function(i, tris) {
				var val = (tris.prop.value) || "";
				var val2 = (tris.s.value) || "";
				val = getStringFromURI(val);
				val2 = getStringFromURI(val2);

				var old = $('#editorBox_dbpediaInfoDiv').html();
				if (old.length == 0) {
					old = "<b>GENERAL</b>";
				}

				//if geo property, show gmap
				if (val == "http://www.georss.org/georss/point") {
					val2 = val2 + ' <div id="dbpediaMap"></div> ';
				}

				$('#editorBox_dbpediaInfoDiv').html(old + "<br/>-<b>" + val + "</b>: " + val2).addClass('editorDiv');

				if (val == "http://www.georss.org/georss/point") {
					var ll = val2.split(" ");
					$("#dbpediaMap").gMap({
						markers : [{
							latitude : ll[0], //47.660937, //46.04888888888889
							longitude : ll[1], //9.569803, //14.50388888888889
							popup : true
						}],	zoom : 15}); //6
				}
				
			});
		
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			//$('#editorBox_infoDiv-GENERAL').html("GENERAL:error");
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	}); //ajax
}



////////////////////////////////////
////// add new NODE/ENTITY in GRAPH

// request data of selected node/entity + show it on a graph = addSelectedNodeToGraphMAIN
function dbCon_addSelectedNodeToGraph_newInCenter(value) {
	dbCon_addSelectedNodeToGraphMAIN(value, "clean"); //status = "clean" - clean old graph
}

function dbCon_addSelectedNodeToGraph_appendToExistingGraph(value) {
	dbCon_addSelectedNodeToGraphMAIN(value, "add"); //status = "add" -append to existing graph
}



function dbCon_addSelectedNodeToGraphMAIN(value, status) {

	var depthInput = $('#editorBox_graphicsDiv-depth-input').val();
	// get values from radio buttons
	var depthType = $('input:radio[name=editorBox_graphicsDiv-depthType-radio]:checked').val();
	var propertiesType = $('input:radio[name=editorBox_graphicsDiv-properties-radio]:checked').val();

	depthInput -= 1;
	var depth;	

	if (depthType == "leq") {
		depth = "," + depthInput;
	} else {
		depth = depthInput;
	}

	// only connections among ObjectProperty! - INverseOf properties are eliminated
	// it is hardcoded because of speed and lightness of UI - find better, automated, solution
	var qText = ['PREFIX d: <http://www.lavbic.net/owl/AJPES.owl#>		', 
	' 														', 
	' SELECT ?p ?r ?s										', 
	' { <' + (value) + '>										', 
	'   (													',
	//'  owl:inverseOf                                        |	',
	//' rdfs:domain                                           |	',
	//' rdfs:subPropertyOf                                    |	',
	//' rdfs:range                                            |	',
	//' rdfs:subClassOf                                       |	',
	//' d:poslovniSubjektImaImetnikaVDruzbeniku               |	',
	' d:poslovniSubjektImaSedezNaNaslovu                    |	', 
	' d:druzbenikJeImetnikPoslovnegaSubjekta                |	', 
	' d:druzbenikJeRegistriranNaNaslovu                     |	',
	//' d:naslovJeRegistriranPriDruzbeniku                    |	',
	//' d:naslovJeSedezPoslovnegaSubjekta                     |	',
	//' d:naslovJeRegistriranPriZastopniku                    |	',
	' d:poslovniSubjektZastopaZastopnik                     |	', 
	' d:druzbenikImaPoslovniDelez                           |	',
	//' d:poslovniDelezVeljaZaDruzbenika                      |	',
	' d:zastopnikJePooblascenZaZastopanjePoslovnegaSubjekta |	', 
	' d:zastopnikJeRegistriranNaNaslovu 					|	', 
	' d:vlogaJePovezanaZNaslovom 					|	', 
	' d:vlogaJePovezanaZVlogo 						', 
	'	  						', 
	'  ){' + depth + '} ?s.		', 
	'  ?p ?r ?s.				', 
	'  }   						'].join('\n');

	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			nodeInfo_lastJson = data;

			var data_tmp = {};

			$.each(data.results.bindings, function(i, tris) {
				data_tmp[  getStringFromURI(  tris[data.head.vars[0]].value) + "->" + getStringFromURI(tris[data.head.vars[2]].value)  ] = {
					from : "" + (tris[data.head.vars[0]].value) + "",
					property : "" + (tris[data.head.vars[1]].value) + "",
					to : "" + tris[data.head.vars[2]].value + ""
				};
			});

			//status = "clean" - clean old graph | "add" -append to existing graph
			general_pushSparqlDataToArbor(data_tmp, status);

		}, 
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			$('#queryStatus').addClass('error').html("XMLHttpRequest=" + XMLHttpRequest.responseText + "\ntextStatus=" + textStatus + "\nerrorThrown=" + errorThrown);
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax

}



///////////////////////////////////////////////////////////////////
/////////////// SEARCH PAD //////////////////////////////////////////


//////////////////////////////////
/// main Search/Filter  functions

// get list of suggestions on search request | SEARCH PAD - "search by person/company/institution"
function dbCon_getFilterSuggestions(txt) {
	var filtercond = "";
	//input to  words
	var txtArray = txt.split(' ');

	//words to regex+str
	$.each(txtArray, function(index, value) {

		filtercond += 'regex(str(?l), \'' + value + '\',"i")';
		if (index < (txtArray.length - 1)) {
			filtercond += " && ";
		}
	});

	var qText = [
	'SELECT ?s  ?l  (COUNT(?s) AS ?count)  WHERE {						', 
	'?someobj ?p ?s .													', 
	'?s d:naziv ?l														', 
	'																	', 
	'FILTER ( ' + filtercond + ' ).										', 
	'																		', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2001/XMLSchema\')).		', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/1999/02/22-rdf-syntax-ns\')).', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2000/01/rdf-schema\')).	', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2002/07/owl\')).			', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2005/xpath-functions\')).	', 
	'FILTER (!isLiteral(?someobj)).	', 
	'}								', 
	'GROUP BY ?s ?l					', 
	'ORDER BY DESC(?count) 			', 
	'								'].join('\n');

if(txt.length<3){
	$('#editor_filter_suggests').html(':error: please enter at least 3 chars long string');
	return;
}else{
	$('#editor_filter_suggests').html('verifying...');
}

	var inHtml = "";

	//send request
	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			try {
				nodeInfo_lastJson = data;
				
				var el = data.results.bindings;
				var countData = el[0][data.head.vars[2]].value;	//== "count"
				if (countData == "0") {//parseInt('123.45'
					throw "incorrectly entered data or no data found";
				}

				$.each(data.results.bindings, function(i, tris) {
					inHtml += '<option value="' + tris[data.head.vars[0]].value + '">' + tris[data.head.vars[1]].value + '</option>';
				});

				$('#editor_filter_suggests').html('<select id="select_filter_res" name="select_filter_res" size="5"   > ' + inHtml + '</select>');

				// !! add triggers to new elements
				$("#select_filter_res").change(function() {
					var ts = $(this);
					var lab = getStringFromURI($(this).val());
					selectedNode['label'] = lab;
					//get label from DOM
					selectedNode['uriId'] = $(this).val();
					selectedNode['node'] = "";

					$('#selectedNode').html("Selected: " + lab);
				});
				
			} catch(e) {
				$('#editor_filter_suggests').html(':' + e);
			}

		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			// $('#queryStatus').addClass('error').html("(getPropertiesList)  XMLHttpRequest="+XMLHttpRequest.responseText+"\ntextStatus="+textStatus+"\nerrorThrown="+errorThrown);
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//end ajax
}



// get list of suggestions on search request by defined property | SEARCH PAD - "search by properties" 
function dbCon_propertySuggestion(thisVar, inputId, inputDivSug, txt) {
	 //e.g. inputId= filter_addedPropID_davcnaStevilka  , inputDivSug=inputDivSug . txt=npr. Si10
	
	var filtercond = ""; 
	
	//get data from data-* attributes - inside input 
	var typeFullURI = thisVar.attr('data-propertyuri');
	var classFullURI = thisVar.attr('data-classuri');
	
	var qtxtAddclass = classFullURI == "" ? "" : ("?s ?n  <" + classFullURI + ">. " );
	
	var type = inputId.substring(inputId.lastIndexOf("_") + 1, inputId.length);
	
	//put input string to separate regex words
	var txtArray = txt.split(' ');
	var txtArrayLen = txtArray.length;

	$.each(txtArray, function(index, value) {
		filtercond += 'regex(str(?l), \'' + value + '\',"i")';
		
		if (index < (txtArray.length - 1)) {
			filtercond += " && ";
		}

	});
     
	var qText = [
	'SELECT ?s  ?l  (COUNT(?s) AS ?count)  WHERE {		', 
	'?someobj ?p ?s .									',
	//'?s d:' + type + ' ?l								',
	'?s <' + typeFullURI + '> ?l.								', 
	' ' + qtxtAddclass + ' ', '													', 
	'FILTER ( ' + filtercond + ' ).					', 
	'													', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2001/XMLSchema\')).			', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/1999/02/22-rdf-syntax-ns\')).	', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2000/01/rdf-schema\')).		', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2002/07/owl\')).				', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2005/xpath-functions\')).		', 
	'FILTER (!isLiteral(?someobj)).												', '} 																			', 
	'GROUP BY ?s ?l																', 
	'ORDER BY DESC(?count) 														', 
	'LIMIT 20	'].join('\n');

	var inHtml = "";
	$("#" + inputDivSug).addClass("propertySug");
	//send request
	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			try {

				var el = data.results.bindings;
				var countData = el[0][data.head.vars[2]].value;
				if (countData == "0") {
					throw "incorrectly entered data or no data found";
				}

				$.each(data.results.bindings, function(i, tris) {
					inHtml += '<option value="' + getStringFromURI(tris[data.head.vars[1]].value) + '">' + getStringFromURI(tris[data.head.vars[0]].value) + '</option>';
				});

				$('#' + inputDivSug).html('<select id="select_' + inputDivSug + '" name="select_' + inputDivSug + '" size="5"   > ' + inHtml + '</select>');

				// !! add triggers to new elements
				$("#select_" + inputDivSug).change(function() {
					var ts = $(this);
					var lab = getStringFromURI($(this).val());

					$('input[name="' + inputId + '"]').val(lab);

					lab = getStringFromURI($(this).children(":selected").text());
					//lab = getStringFromURI($(this).text());
					selectedNode['label'] = lab;
					//get label from DOM
					selectedNode['uriId'] = 'http://www.lavbic.net/owl/AJPES.owl#' + lab;
					selectedNode['node'] = "";

					$('#selectedNode').html("Selected: " + lab);

				});

				$('#' + inputDivSug).click(function() {
					$("#select_" + inputDivSug).change();
				});

			} catch(e) {
				$('#' + inputDivSug).html(':' + e);
				$("#" + inputDivSug).removeClass("propertySug");
			}
		},//OnSuccess
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			$('#' + inputDivSug).html('errorOnError:' + textStatus + " >" + errorThrown);
		}//OnError
		,
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax
}



// get list of suggestions by all selected properties | SEARCH PAD - "search by properties" 
function dbCon_searchByAllProperties() {
	// mainDivId: filter_addedProperties
	// class: filter_addedProp_cell
	
	//end func. if no selected properties
	if ( !$.trim( $('#filter_addedProperties').html() ).length){
		$("#editor_searchByAllProperties_suggests").html("error: please add at least one property");
		return;}
	
	$("#editor_searchByAllProperties_suggests").html("verifying...");
	var filterQuery = "";

	$(".filter_addedProp_cell > input").each(function(indexProp, el) {

		var filtercond = "";
		//get data from data-* attributes - inside <input> 
		var typeFullURI = $(this).attr('data-propertyuri');
		var classFullURI = $(this).attr('data-classuri');

		var qtxtAddclass = classFullURI == "" ? "" : ("?s ?n" + indexProp + "  <" + classFullURI + ">. " );
				
		//put input string to separate regex words
		var txt = $(this).attr('value');
		var txtArray = txt.split(' ');

		$.each(txtArray, function(index, value) {
			filtercond += 'regex(str(?l' + indexProp + '), \'' + value + '\',"i")';

			if (index < (txtArray.length - 1)) {
				filtercond += " && ";
			}
		});

		filterQuery += ' ?s <' + typeFullURI + '> ?l' + indexProp + '.	';
		filterQuery += ' ' + qtxtAddclass + ' ';
		filterQuery += ' FILTER ( ' + filtercond + ' ). ';

	});
	//end each func.- over all attributes

	
	var qText = [
	'SELECT ?s  ?l0  (COUNT(?s) AS ?count)  WHERE {		', 
	'?someobj ?p ?s .									', 
	' ' + filterQuery + ' ', '													', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2001/XMLSchema\')).			', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/1999/02/22-rdf-syntax-ns\')).	', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2000/01/rdf-schema\')).		', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2002/07/owl\')).				', 
	'FILTER (!regex(str(?s), \'\^http://www.w3.org/2005/xpath-functions\')).		', 
	'FILTER (!isLiteral(?someobj)).												', 
	'} 																			', 
	'GROUP BY ?s ?l0																', 
	'ORDER BY DESC(?count) 														', 
	'LIMIT 20	'].join('\n');

	$('#editor_searchByAllProperties_suggests').html('verifying...');
	var inHtml = "";
	//send request
	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			try {
				nodeInfo_lastJson = data;
				var el = data.results.bindings;
				var countData = el[0][data.head.vars[2]].value;				
				if (countData == "0") {
					throw "incorrectly entered data or no data found";
				}

				$.each(data.results.bindings, function(i, tris) {
					inHtml += '<option value="' + tris[data.head.vars[0]].value + '">' + getStringFromURI(tris[data.head.vars[0]].value) + '</option>';
				});

				$('#editor_searchByAllProperties_suggests').html('<select id="select_searchByAllProperties_res" name="select_searchByAllProperties_res" size="5"   > ' + inHtml + '</select>');

				// !! add triggers to new elements
				$("#select_searchByAllProperties_res").change(function() {
					var ts = $(this);
					var lab = getStringFromURI($(this).val());
					selectedNode['label'] = lab;
					//get label from DOM
					selectedNode['uriId'] = $(this).val();
					selectedNode['node'] = "";

					$('#selectedNode').html("Selected: " + lab);
				});


			} catch(e) {
				$('#editor_searchByAllProperties_suggests').html(':' + e);
			}

		}, //OnSuccess
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			// $('#queryStatus').addClass('error').html("(getPropertiesList)  XMLHttpRequest="+XMLHttpRequest.responseText+"\ntextStatus="+textStatus+"\nerrorThrown="+errorThrown);
			$('#' + inputDivSug).html('errorOnError:' + textStatus + " >" + errorThrown);
		},//OnError
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax
}



//////////////////////////////////////////
/// Get value of NODE's special property 
 
function dbCon_getNode_type(uriId) {
	return dbCon_getNode_specialPROPERTYvalue(uriId, 'rdf:type');
}

function dbCon_getNode_skrajsanNaziv(uriId) {
	return dbCon_getNode_specialPROPERTYvalue(uriId, 'd:skrajsanNaziv');
}

function dbCon_getNode_naziv(uriId) {
	return dbCon_getNode_specialPROPERTYvalue(uriId, 'd:naziv');
}

//Get property value of defined uriID | dProperty = d:naziv, d:skrajsanNaziv, rdf:type,...
function dbCon_getNode_specialPROPERTYvalue(uriId, dProperty) {
	var value = getStringFromURI(uriId);

	var qText = [
	'SELECT  ?val { 	', 
	'<' + uriId + '> ' + dProperty + ' ?val		',
	'}	'].join('\n');
	
	var label;
	runAjaxLoad();
	try {
		$.ajax({
			url : endpointTab[selectedEndpointTab].url,
			data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
			async : false,
			dataType : "json",
			type : "GET",
			success : function(data) {
				label = data.results.bindings[0].val.value;
			}, 
			error : function() {
				label = "";
			},
			complete : function() {
			},
			beforeSend : function(xhr) {
			}
		});

		label = getStringFromURI(label);
	} catch(e) {
		label = "";
	}
	stopAjaxLoad();
	return label;
}


/////////////////////////////////////////
/// Get list of  CLASSes and PROPERTIes

// load all classes from  rdf database to <option> list | SEARCH PAD - "search by properties" (filter box)
function dbCon_loadClassesList() {
	$('#select_classList').html("...loading");
	$('#select_propertyList').html("");

	var qText = ['SELECT  ?s ?l  ', 
	'{              ', 
	' ?s rdf:type owl:Class   .  ', 
	' ?s rdfs:label   ?l .  	', 
	'} '].join('\n');

	var inHtml = "";
	//send request
	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			nodeInfo_lastJson = data;
			$.each(data.results.bindings, function(i, tris) {
				inHtml += '<option value="' + tris[data.head.vars[0]]['value'] + '">' + tris[data.head.vars[1]]['value'] + '</option>';
			});

			$('#select_classList').html(inHtml);

			//!! add triggers to new elements
			$("#select_classList").change(function() {
				var fullclassname = $(this).val();
				dbCon_loadPropertiesOfClassList(fullclassname);
			});
		}, error : function(XMLHttpRequest, textStatus, errorThrown) {
			//$('#queryStatus').addClass('error').html("(getPropertiesList)  XMLHttpRequest="+XMLHttpRequest.responseText+"\ntextStatus="+textStatus+"\nerrorThrown="+errorThrown);
		}, complete : function() {
			stopAjaxLoad();
		}, beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax
}


// load all properties of defined class name from rdf databese to <option> list | SEARCH PAD - "search by properties" (filter box)
function dbCon_loadPropertiesOfClassList(fullClassName) {
	var className = getStringFromURI(fullClassName);
	var qText = ['SELECT DISTINCT ?r ', 'WHERE { 			',
	'  ?s rdf:type <' + fullClassName + '>. 	', 
	'   ?s  ?r ?t.		', 
	'}  				'].join('\n');
	
	var inHtml = "";
	//send request
	$.ajax({
		url : endpointTab[selectedEndpointTab].url,
		data : "query=" + encodeMyURI(urlQueryGeneralPrefix + qText) + urlQueryType,
		dataType : "json",
		type : "GET",
		success : function(data) {
			nodeInfo_lastJson = data;
			$.each(data.results.bindings, function(i, tris) {
				inHtml += '<option value="' + tris[data.head.vars[0]]['value'] + '">' + getStringFromURI(tris[data.head.vars[0]]['value']) + '</option>';
			});

			$('#select_propertyList').html(inHtml);

			//!! add triggers to new elements
			$("#select_propertyList").change(function() {
				var ts = $(this);
				var vall = $(this).val();
			});
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			//$('#queryStatus').addClass('error').html("(getPropertiesList)  XMLHttpRequest="+XMLHttpRequest.responseText+"\ntextStatus="+textStatus+"\nerrorThrown="+errorThrown);
		},
		complete : function() {
			stopAjaxLoad();
		},
		beforeSend : function(xhr) {
			runAjaxLoad();
		}
	});	//ajax
}




///////////////////////////////////////////////////////////
/////////  QUERY PAD  /////////////////////////////////////

//get JSON from custom query
function dbCon_submitQueryGetJSON(qText) {
		//encode text to url
		qText = encodeMyURI(qText);	
		//send request
		$.ajax({
			url : endpointTab[selectedEndpointTab].url,
			data : "query=" + qText + urlQueryType,
			dataType : "json",
			type : "GET",
			success : function(data) {
				var popUpWin = window.open('', 'name', 'height=400,width=500,scrollbars=yes');
				popUpWin.document.write('<pre style="word-wrap: break-word; white-space: pre-wrap;">' + JSON.stringify(data, null, 4) + '</pre>');
				popUpWin.document.close();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$('#queryStatus').addClass('error').html("XMLHttpRequest=" + XMLHttpRequest.responseText + "\ntextStatus=" + textStatus + "\nerrorThrown=" + errorThrown);
			},
			complete : function() {
				stopAjaxLoad();
			},
			beforeSend : function(xhr) {
				if (endpointTab[selectedEndpointTab].needAuthorization) {
					xhr.setRequestHeader("Authorization", endpointTab[selectedEndpointTab].authorization);
					xhr.setRequestHeader('Accept', 'application/sparql-results+json');
				}
				runAjaxLoad();
			}
		});	//end ajax
}

//get graph(on canvas) from custom query
function dbCon_submitQuery(qText){
  		//encode text to url
		qText = encodeMyURI(qText);
		//send request
		$.ajax({
			url : endpointTab[selectedEndpointTab].url,
			data : "query=" + qText + urlQueryType,
			dataType : "json",
			type : "GET",
			success : function(data) {
				try {
					nodeInfo_lastJson = data;
					var data_tmp = {};

					$.each(data.results.bindings, function(i, tris) {
						data_tmp[getStringFromURI(tris[data.head.vars[0]].value) + "->" + getStringFromURI(tris[data.head.vars[2]].value)] = {
							from : "" + (tris[data.head.vars[0]].value) + "",
							property : "" + (tris[data.head.vars[1]].value) + "",
							to : "" + tris[data.head.vars[2]].value + ""
						};
					});
					//status = "clean" - clean old graph | "add" -append to existing graph
					general_pushSparqlDataToArbor(data_tmp, "clean");
			
				} catch(e) {
					$('#queryStatus').addClass('error').html("status:no results... " + e);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$('#queryStatus').addClass('error').html("XMLHttpRequest=" + XMLHttpRequest.responseText + "\ntextStatus=" + textStatus + "\nerrorThrown=" + errorThrown);
			},
			complete : function() {
				stopAjaxLoad();
			},
			beforeSend : function(xhr) {
				if (endpointTab[selectedEndpointTab].needAuthorization) {
					xhr.setRequestHeader("Authorization", endpointTab[selectedEndpointTab].authorization);
					xhr.setRequestHeader('Accept', 'application/sparql-results+json');
				}
				runAjaxLoad();
			}
		});	//end ajax
}




