/*
 *	General.js 
 * 
 * -main page functionality
 * -DOM/HTML manipulation
 * -CSS manipulation
 * -user interaction logic
 * 
 * Created by GregaS.
 */

var globalHalfViz_that;
var arborTxt = "";

//show ajax loading img, until =0
var ajaxRunningNumb = 0;

//get json from last sparql - for info/debugging
var nodeInfo_lastJson;

//for endpoint selector
var selectedEndpointTab = "local";
var urlQueryEndpoint = endpointTab[selectedEndpointTab].url; //e.g. http://localhost:3030/ds/query

// sparql -> rdf/json -> dataArbor -> arbor(visual graph)
var dataArbor_rdftable={}; //table row - {from:,property:,to:}
var dataArbor_nodes={}; 
var dataArbor_edges={};
var dataArbor_showOnlyThisProperties={};

// important element - many functions use it - search/filter results, canvas click, info of,...
var selectedNode = new Array();
selectedNode['label'] = "";
selectedNode['uriId'] = "";
selectedNode['node'] = ""; //null;

// prefix list - sparql query
var urlQueryGeneralPrefix = [
'PREFIX xsd:     <http://www.w3.org/2001/XMLSchema#>	',
 'PREFIX rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ',
  'PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>	', 
  'PREFIX owl:     <http://www.w3.org/2002/07/owl#>		', 
  'PREFIX fn:      <http://www.w3.org/2005/xpath-functions#>', 
  'PREFIX apf:     <http://jena.hpl.hp.com/ARQ/property#>	', 
  'PREFIX dc:      <http://purl.org/dc/elements/1.1/>', 
  'PREFIX dbpedia: <http://dbpedia.org/ontology/>		',
  'PREFIX d: 	<http://www.lavbic.net/owl/AJPES.owl#>	',
  ' '].join('\n');
var urlQueryType = '&output=json';

//for addPropertyToFilter(...) and addPropertyANDClassToFilterFromEntityInfo(...) [ add property (and class) to filter list ]
selectedEntityInfo_uriId = "";


$(document).ready(function() {
	//init load

	//disable browser's right-click contextmenu, show only on Canvas area (viewport)
	$('body').on('contextmenu', '#viewport', function(e){ return false; });

	//hide custom contextmenu, after mouseleave - on canvas 
	$("#selectedNode_canvasContextMenu").mouseleave(function() {
  		$(this).hide();
  	});
	
	//hide all right-slider boxes
	$('.editorBox').hide();
	
	//load and start arbour
	$('#code').html(arborTxt);
	
	globalHalfViz_that.updateDoc(arborTxt);
	globalHalfViz_that.updateLayout(Math.max(1, $(window).width() - 30))
	globalHalfViz_that.resize()
	
	///Experimental ------------		
		// EndPointSELECTOR
		var selectHtml = "";
		$.each(endpointTab, function(k, v) {//index =k, obj.=v
			selectHtml += '<option value="' + k + '">' + k + " > " + v.url + '</option>';
		});
		//append option list to html/dom
		$('#select_endpointsList').html(selectHtml);
		//!! add triggers to new elements
		$("#select_endpointsList").change(function() {
			selectedEndpointTab = $(this).val();
			urlQueryEndpoint = endpointTab[selectedEndpointTab].url; //e.g. http://localhost:3030/ds/query
			
			alert('change endpoint:' + $(this).val());
	
			$("#filter_addedProperties").html();
			dbCon_loadClassesList();
			alert('ok, endpoint changed, class list loaded:' + $(this).val());
		});
	///------------------
			

	// TOP-LEFT ///////////////////////////////////////////////////////////////////////

	var opacityPopUp=0.97;
	//// TOP-LEFT -- DASHBOARD - activate popup box
	$('#searchImg').click(function() {//show IntroSearch
		$('.popupBox').hide();
		var intro = $("#introSearch")
		if (intro.css('display') == 'block')
			return false

		intro.stop(true).css('opacity', 0).show().fadeTo('fast', opacityPopUp)
		return false

	});

	$("#introSearch h1 a").click(function() {//hide IntroSearch
		var intro = $("#introSearch");
		if (intro.css('opacity') < opacityPopUp)
			return false;

		intro.stop(true).fadeTo('fast', 0, function() {
			intro.hide();
		});
		return false
	});

	$('#queryImg').click(function() {//show IntroQuery
		$('.popupBox').hide();
		var intro = $("#introQuery")
		if (intro.css('display') == 'block')
			return false

		intro.stop(true).css('opacity', 0).show().fadeTo('fast', opacityPopUp)
		return false
	});

	$("#introQuery h1 a").click(function() {//hide IntroQuery
		var intro = $("#introQuery");
		if (intro.css('opacity') < opacityPopUp)
			return false;

		intro.stop(true).fadeTo('fast', 0, function() {
			intro.hide();
		});
		return false
	});

	$('#viewImg').click(function() {//show ViewImg
		$('.popupBox').hide();
		var intro = $("#introView")
		if (intro.css('display') == 'block')
			return false

		intro.stop(true).css('opacity', 0).show().fadeTo('fast', opacityPopUp)
		return false
	});

	$("#introView h1 a").click(function() {//hide ViewImg
		var intro = $("#introView");
		if (intro.css('opacity') < opacityPopUp)
			return false;

		intro.stop(true).fadeTo('fast', 0, function() {
			intro.hide();
		});
		return false
	});


	//// TOP-LEFT -- Dashboard POPUP box - SEARCH PAD ////

	// slide up/down #tabs#
	$('.accordion').find('h2').click(function() {
		$(this).siblings('h2').next().slideUp();
		// closes siblings
		$(this).next().slideToggle("slow");
	}).next().hide();

	//search by person/company/institution/
	$("#btnID_searchByEntityName").click(function() {
		var txt = $('input[name=inputID_filter]').val();
		dbCon_getFilterSuggestions(txt);
	});
	
	$('input[name=inputID_filter]').keyup(function(e) {
		var code = e.which;
		// recommended to use e.which, it's normalized across browsers
		if (code == 13)
			e.preventDefault();
		if (code == 13) { // enter-key
			var txt = $(this).val();
			dbCon_getFilterSuggestions(txt);
		}
	}).focus(function() {
		$('#editor_filter_suggests').show();
	});

	$('#editor_filter_suggests').focusout(function() {
		$('#editor_filter_suggests').hide();
	})
	
	////////////////////////////////
	/// filter/search by properties
	
	// btn to add selected property in filter list
	$("#btnID_addProperty").click(function() {
		var newProp = getStringFromURI($('#select_propertyList').val());
		var newFullURIprop = $('#select_propertyList').val();

		addPropertyToFilter(newProp, newFullURIprop, "", "");
	});
	// 
	$("#btnID_addClassANDProperty").click(function() {
		var newProp = getStringFromURI($('#select_propertyList').val());
		var newFullURIprop = $('#select_propertyList').val();

		var newClass = getStringFromURI($('#select_classList').val());
		var newFullURIclass = $('#select_classList').val();

		addPropertyToFilter(newProp, newFullURIprop, newClass, newFullURIclass);
	});

	// btn to filter/search by ALL listed properties
	$("#btnID_searchByAllProperties").click(function() {
		dbCon_searchByAllProperties();
	});
	
	
	//// TOP-LEFT -- Dashboard POPUP box - Query pad ////
	
	var qText_textarea=" \n SELECT *{ \n ?s ?x ?p \n } \n LIMIT 40 \n";
	$("#queryText").val(urlQueryGeneralPrefix + qText_textarea);
	
	//btn to run CUSTOM sparql QUERY - show JSON results in popup window
	$('#submitQueryGetJSON').click(function() {
		var qText = $("#queryText").val();
		dbCon_submitQueryGetJSON(qText);
	});	

	//btn to run CUSTOM sparql QUERY - show results on canvas(graphically)
	$('#submitQuery').click(function() {
		var qText = $("#queryText").val();
		dbCon_submitQuery(qText);	
	});	
	
	
	//// TOP-LEFT -- Dashboard POPUP box - View pad ////

	$("#showSubclassStructure").click(function() {
		dbCon_showSubclasses_InArbor();
	});

	$('#showNodeInfo_lastJson').click(function() {
		var popUpWin2 = window.open('', 'name', 'height=400,width=500,scrollbars=yes');
		popUpWin2.document.write('<pre style="word-wrap: break-word; white-space: pre-wrap;">' + JSON.stringify(nodeInfo_lastJson, null, 4) + '</pre>');
		popUpWin2.document.close();
	});
	
	
	//manualy start/stop simulation. By default the system will run and pause on its own based on the level of energy in the particles.
	$('#sysStart').click(function() {
		sys.start();
	});
	$('#sysStop').click(function() {
		sys.stop();
	});
	
	//// RIGHT - EditorBOX(infoBox) -- show entity in graph() ////

	$("#editorBox_graphicsDiv-depth-slider").slider({
		value : 1,
		min : 1,
		max : 10,
		step : 1,
		slide : function(event, ui) {
			$("#editorBox_graphicsDiv-depth-input").val(ui.value);
		}
	});

	$("#editorBox_graphicsDiv-depthType-radio").buttonset();	// radio btn

	$("#editorBox_graphicsDiv-properties-radio").buttonset();	// radio btn

	$("#showNode_newInCenter").click(function() {
		dbCon_addSelectedNodeToGraph_newInCenter(selectedNode['uriId']);
	});

	$("#showNode_appendToExistingGraph").click(function() {
		dbCon_addSelectedNodeToGraph_appendToExistingGraph(selectedNode['uriId']);
	});

	//// BOTTOM - statusBox AND canvasContextMenu /////////////////////////////////////
	
	$('[id^="showFullNodeInfo"]').click(function() { //partial id selector! 
		$('.editorBox').hide();
		$('#editorBox_info').show();

		globalHalfViz_that.updateLayout(Math.max(1, $(window).width() - 340));
		globalHalfViz_that.resize();
		
		dbCon_nodeInfoFull(selectedNode['uriId'], selectedNode['node']);
	});

	$('[id^="showNodeGraphics"]').click(function() {//partial id selector! 
		$('.editorBox').hide();
		$('#editorBox_graphics').show();

		globalHalfViz_that.updateLayout(Math.max(1, $(window).width() - 340));
		globalHalfViz_that.resize();
	});

	$('[id^="showNode_dbpediaInfo"]').click(function() {//partial id selector! 
		$('.editorBox').hide();
		$('#editorBox_dbpediaInfo').show();

		globalHalfViz_that.updateLayout(Math.max(1, $(window).width() - 340));
		globalHalfViz_that.resize();
		
		dbCon_nodeDBpediaInfo(selectedNode['uriId'], selectedNode['node']);
	});
	

	$('#showNodesPathFinder').click(function() {
		$('.editorBox').hide();
		$('#editorBox_nodesPathFinder').show();

		globalHalfViz_that.updateLayout(Math.max(1, $(window).width() - 340));
		globalHalfViz_that.resize();
	});

	//add triggers to <input>
	restrictInput_Alphanumeric();
	
}); //////////////////////////////   end of  - on document ready ////////////////////////////////////



///////////////////////////////// GLOBAL STATIC FUNCTIONS ///////////////////////////////////



// RESTRICT INPUT (improve this!!)- allow alphanumeric(+ space) only = call this func. after user insert/append new input string in dom/html
function restrictInput_Alphanumeric () {
	$('input[type="search"]').bind('keyup', function (event) {
	    var txt = $(this).val();
	  	$(this).val( txt.replace(/[^a-z0-9 _čČšŠžŽđĐ\s]/gi, '') ); 
	});
}


////SEARCH PAD// Add/Remove property + class to/from filter list  ////

//SearchPad// - add property from option list to filter list = main function | (class->prop) -> search
function addPropertyToFilter(newProp, newFullURIprop, newClass, newFullURIclass) {
	var classtxt = newClass == "" ? newClass : (newClass + " &#8674; ");

	//verify if already added, append only if not exist !
	if ($('input[name="filter_addedPropID_' + newClass + '' + newProp + '"]').length == 0) { //'x' change for &times; 
		$("#filter_addedProperties").append('<div class="filter_addedProp_cell">' + classtxt + '' + newProp + '<br /><a href="#" onclick="removeThisPropertyFromFilter(this);" >x</a><input name="filter_addedPropID_' + newClass + '' + newProp + '" data-propertyuri="' + newFullURIprop + '"  data-classuri="' + newFullURIclass + '"   type="search"/> <div class="propertySug" id="filter_addedPropID_' + newClass + '' + newProp + '_divSug" ></div>	</div>');
	} else {
		return;
	}

	restrictInput_Alphanumeric();
	
	$('input[name="filter_addedPropID_' + newClass + '' + newProp + '"]').keyup(function(e) {
		var code = e.which;
		// recommended to use e.which, it's normalized across browsers
		if (code == 13)
			e.preventDefault();
		if (code == 13) {  //detect enter-key
			var txt = $(this).val();
			var attr = $(this).attr('name');

			$("#" + attr + "_divSug").html("verifying ...").show();
			
			dbCon_propertySuggestion($(this), attr, attr + "_divSug", txt);

		} // missing closing if brace
	}).focus(function() {
		$('#filter_addedPropID_' + newClass + '' + newProp + '_divSug').show();
	});
	

	$('#filter_addedPropID_' + newClass + '' + newProp + '_divSug').focusout(function() {
		$('#filter_addedPropID_' + newClass + '' + newProp + '_divSug').hide();
	});

}

//SearchPad// - remove  property  from filter list - remove parent div
function removeThisPropertyFromFilter(item) {
	$(item).parent().remove(); 	
}


//SearchPad// - add property to filter list(div)
function addPropertyToFilterFromEntityInfo(propertyName, propertyUriId, propertyFullURI) {
	var newProp = propertyName;
	
	addPropertyToFilter(propertyName, propertyFullURI, "", "");

	// Slide up/down #accordion#
	$("#accordion_searchByEntityProperties").siblings('h2').next().slideUp();
	$("#accordion_searchByEntityProperties").slideDown();

	$('.popupBox').hide();
	var intro = $("#introSearch")
	if (intro.css('display') == 'block')
		return false

	intro.stop(true).css('opacity', 0).show().fadeTo('fast', 1)
	return false
}

//SearchPad//- add property AND class to  filter list(div) 
function addPropertyANDClassToFilterFromEntityInfo(propertyName, propertyUriId, propertyFullURI) {
	var newProp = propertyName;
	
	addPropertyToFilter(propertyName, propertyFullURI, getStringFromURI(selectedEntityInfo_uriId), selectedEntityInfo_uriId);

	// Slide up/down #accordion#
	$("#accordion_searchByEntityProperties").siblings('h2').next().slideUp();
	$("#accordion_searchByEntityProperties").slideDown();

	$('.popupBox').hide();
	var intro = $("#introSearch")
	if (intro.css('display') == 'block')
		return false

	intro.stop(true).css('opacity', 0).show().fadeTo('fast', 1)
	return false
}





// ArborJs call this func.- click on canvas(graph nodes)
function nodeClickedTrigger(node, action,e) {
	selectedNode['label'] = node.data.label;
	selectedNode['uriId'] = node.data.uriId;
	selectedNode['node'] = node;

	$('#selectedNode').html("Selected: " + node.data.label);

 	if( e.which == 1 ) {
    //left mouse click
   }else if( e.which == 3 ) {
    //right mouse click
    onRightClick_showCanvasContextMenu(e);
   }else if( e.which == 2 ) {
    //middle mouse click
   }
   e.preventDefault();
	
	return
}

// right-click on canvas - load and show custom contextmenu
function onRightClick_showCanvasContextMenu (e) {
	var popBox_div=$("#selectedNode_canvasContextMenu");
	var popBox_name_div=$("#selectedNode_canvasContextMenu > div.name");
	
	var uriId=selectedNode['uriId'] ;
	var html="<b>::"+selectedNode['label'] +"</b><br/>";
	
	var label=dbCon_getNode_specialPROPERTYvalue(uriId,'rdfs:label');;
	if(label.length>0){
		html+="<b>:label:</b> "+label +"<br/>";
	}
	
	var skrajsanNaziv=dbCon_getNode_specialPROPERTYvalue(uriId,'d:skrajsanNaziv');//dbCon_getNode_skrajsanNaziv(uriId);
	if(skrajsanNaziv.length>0){
		html+="<b>:skrajsanNaziv:</b> "+skrajsanNaziv +"<br/>";
	}
	
	var comment=dbCon_getNode_specialPROPERTYvalue(uriId,'rdfs:comment');
	if(comment.length>0){
		html+="<b>:comment:</b> "+comment +"<br/>";
	}
	
	var naziv=dbCon_getNode_specialPROPERTYvalue(uriId,'d:naziv');
	if(naziv.length>0){
		html+="<b>:naziv:</b> "+naziv +"<br/>";
	}
	
	var type=dbCon_getNode_specialPROPERTYvalue(uriId,'rdf:type');//dbCon_getNode_type(uriId);
	if(type.length>0){
		html+="<b>:type:</b> "+type +"<br/>";
	}
	
	popBox_name_div.html(html);
	popBox_div.show();
	
	popBox_div.offset({left:e.pageX+5,top:e.pageY+5});

}



///////////////// SPARQL to CANVAS - all functions ///////////////

// sparql -> rdf/json -> dataArbor ->arbor (visual graph)
function general_pushSparqlDataToArbor(data_tmp, status) {
	//status = "clean" - clean old graph | "add" -append to existing graph
	//data_tmp = key {from:, property:, to:}
	
	if (status == "clean") {
		dataArbor_rdftable = {}; //this is global var
	}
	
	//MERGE old+new table(of objects)
	dataArbor_rdftable = $.extend({}, dataArbor_rdftable, data_tmp);
	
	//add new el. to dataArbor_nodes[]
	general_pushNewRdfTable_toNodeTab();
	//add new el. to dataArbor_edges[]
	general_pushNewRdfTable_toEdgeTab();
	//add any new property to dataArbor_showOnlyThisProperties[]
	general_pushNewRdfTable_toShowOnlyProp();

	general_redrawArborCanvas();
}


function general_redrawArborCanvas() {
	//1.filter dataArbor_rdftable by dataArbor_showOnlyThisProperties
	//2.write to arborJs format
	//3.show it
	
	var showOnlyThisProperties_tmp = {};
	arborTxt = "";

	for (var key in dataArbor_rdftable) {
		if (dataArbor_rdftable.hasOwnProperty(key)) {
			var tmpRdfRow = dataArbor_rdftable[key];
			showOnlyThisProperties_tmp[tmpRdfRow.property] = 0;

			// only selected properties
			if (dataArbor_showOnlyThisProperties[tmpRdfRow.property] == 1) {
				
				//generate ArborJs format
				showOnlyThisProperties_tmp[tmpRdfRow.property] = 1;
				var arborTmp = "";
				var toNode = dataArbor_nodes[tmpRdfRow.to];
				var fromNode = dataArbor_nodes[tmpRdfRow.from];
				var tmpEdge = dataArbor_edges[tmpRdfRow.property];

				arborTmp += ' node:' + fromNode.id + " -> ";
				arborTmp += ' node:' + toNode.id;

				if (tmpRdfRow.property != "gsEmpty") {
					arborTmp += " {color:" + (tmpEdge.color  ) + ",label:" + (tmpEdge.label  ) + "} \n";
				} else {
					arborTmp += "\n ";
				}

				arborTmp += ' node:' + toNode.id + ' {color:' + toNode.color + ',uriId:' + toNode.uriId + ',label:' + toNode.label + ',labelFull:' + toNode.labelFull + ',type:' + toNode.type + ' } \n';
				arborTmp += ' node:' + fromNode.id + ' {color:' + fromNode.color + ',uriId:' + fromNode.uriId + ',label:' + fromNode.label + ',labelFull:' + fromNode.labelFull + ',type:' + fromNode.type + ' 	} \n';
				arborTxt += arborTmp;
			}
		}
	}

	// add new check box list of properties
	var htmlCheckBoxes = "";
	for (var key in showOnlyThisProperties_tmp) {
		if (showOnlyThisProperties_tmp.hasOwnProperty(key)) {
			htmlCheckBoxes += '<div><input type="checkbox" ' + (((showOnlyThisProperties_tmp[key] == 1) ? "checked" : "") ) + '  id="checkBox_showSelectedProperties_' + getStringFromURI(key) + '" name="checkBox_showSelectedProperties_' + getStringFromURI(key) + '" data-propertyuri="' + key + '"  >' + getStringFromURI(key) + ' </div>';
		}
	}
	//append check box list to html/dom
	$("#arbor_displayProperties").html(htmlCheckBoxes);
	// add triggers
	$("input[type='checkbox'][id^=checkBox_showSelectedProperties_]").change(function() {
		var typeFullURI = $(this).attr('data-propertyuri');
		if ($(this).is(':checked')) {
			dataArbor_showOnlyThisProperties[typeFullURI] = 1;
		} else {
			dataArbor_showOnlyThisProperties[typeFullURI] = 0;
		}
		general_redrawArborCanvas();
	});
	
	
sys.parameters({stiffness:1500, repulsion:2600,gravity:true});//arbor.ParticleSystem(2600, 272, 0.5,true);


	// display it on canvas
	globalHalfViz_that.updateDoc(arborTxt);

	globalHalfViz_that.updateLayout(Math.max(1, $(window).width() - 30))
	
	setTimeout(stiffnessAndRepulsionNormal, 5500);
}

function stiffnessAndRepulsionNormal (argument) {
	sys.parameters({stiffness:272, repulsion:2600,gravity:true});

}

//add any new property to dataArbor_showOnlyThisProperties | default = 1 (show)
function general_pushNewRdfTable_toShowOnlyProp(){
	// through all properties in tab 
	for (var key in dataArbor_rdftable) {
		// if new property, add it. default= 1 (show)
       if(! dataArbor_showOnlyThisProperties.hasOwnProperty(dataArbor_rdftable[key].property) ) {
		dataArbor_showOnlyThisProperties[ dataArbor_rdftable[key].property ]=1;
       }
    }
}


///////////////////////////
//// NODES

//verify if any new From/To node
function general_pushNewRdfTable_toNodeTab(){
	// through all el. in tab 
	for (var key in dataArbor_rdftable) {	
       if(! dataArbor_nodes.hasOwnProperty(dataArbor_rdftable[key].from) ) {
       	// if new FROM, add it.
       	genaral_addNewNode_inNodeTab( dataArbor_rdftable[key].from );
       }
       if(! dataArbor_nodes.hasOwnProperty(dataArbor_rdftable[key].to) ) {
       	// if new TO, add it.
       	genaral_addNewNode_inNodeTab( dataArbor_rdftable[key].to );
       }
    }    
}
//add new el. to object array - dataArbor_nodes[]
function genaral_addNewNode_inNodeTab(uriId){
	//e.g. D_289199_Noc_Tina {color:#95cde5,uriId:http://www.lavbic.net/owl/AJPES.owl#D_289199_Noc_Tina,label:Noč Tina,type:Druzbenik } 
	var node={};
	var labelShortLength=20;
  	node.uriId=uriId;
  	node.id=getStringFromURI(uriId);
  	node.label= dbCon_getNode_skrajsanNaziv(uriId)!="" ?dbCon_getNode_skrajsanNaziv(uriId) : dbCon_getNode_naziv(uriId);//name, shortname,...
  	node.labelFull= node.label= node.label==""?node.id : node.label;//!!! ce je prazno vpise ID
  	if(node.label.length>=labelShortLength){
  		node.label=node.label.substring(0,labelShortLength/2 - 2)+"..."+node.label.substring(node.label.length- labelShortLength/2 +2, node.label.length);
  	}
  	
  	node.type=dbCon_getNode_type(uriId);// type - catergory type
  	node.color= getColorOfNode_ByType(node.type);
  	
	dataArbor_nodes[node.uriId]=node;//{};	
}

function getColorOfNode_ByType(type){
	// class - yellow #D6AE34 , 
	// individual - violet #662265  #7F5BB2, 
	// data property green #32B58B , 
	// objectproperty moder #3E83B2
	//return "#95cde5";// light blue 
	
	switch (type) {
	  case "Class": return "#D6AE34";//| owl:Class   
	    break;
	  case "DatatypeProperty": return "#32B58B";//| owl:DatatypeProperty   
	    break;
	  case "ObjectProperty": return "#3E83B2";//| owl:ObjectProperty 
	    break;
	  case "FunctionalProperty": return "#95cde5"; //???????//| owl:FunctionalProperty
	    break;
	  case "": return "";//grey
	    break;
	  default:  return "#7F5BB2";
	}
  
  //colour map - 
  //light blue {color:#95cde5}, 
  //dark orange   {color:#c6531e}, 
  //light yellow {color:#ffe35f},
  //bordeaux red{color:#b01700} 	
}


///////////////////////////
//// EDGES

//verify if any new property - edge
function general_pushNewRdfTable_toEdgeTab(){
	// through all el. in tab 
	for (var key in dataArbor_rdftable) {
       if(! dataArbor_edges.hasOwnProperty(dataArbor_rdftable[key].property) ) {
       	//add new property to edge table
       	genaral_addNewEdge_inEdgeTab( dataArbor_rdftable[key].property );
       }
    }
     
}
//add new el. to object array - dataArbor_edges[]
function genaral_addNewEdge_inEdgeTab(uriId){
	//e.g. D_289199_Noc_Tina {color:#95cde5,uriId:http://www.lavbic.net/owl/AJPES.owl#D_289199_Noc_Tina,label:Noč Tina,type:Druzbenik } 
	var edge={};
  	edge.uriId=uriId;
  	edge.label=edge.id=getStringFromURI(uriId);
  	//edge.type=dbCon_getNode_type(uriId);// type - catergory type
  	edge.color= "#444";
  	
	dataArbor_edges[edge.uriId]=edge;//{};	
}

////////////////////  end  SPARQL to CANVAS ///////////////////////



// Get short name from URI  | e.g. get Druzbenik from  http://www.lavbic.net/owl/AJPES.owl#Druzbenik
function getStringFromURI(str) {
	str = str.trim();
	if (str.lastIndexOf("#") > 0) {
		str = str.substring(str.lastIndexOf("#") + 1, str.length);
	}
	return str;
}


///  Start/Stop LOADING IMAGE - depend on ajax/sparql requests ///
function runAjaxLoad() {
	ajaxRunningNumb += 1;
	if (ajaxRunningNumb > 0) {
		$('#ajax_loadImg').html('<img src="img/loadcircle.gif" alt=loadImg"" />');
	}
}

function stopAjaxLoad() {
	ajaxRunningNumb -= 1;
	if (ajaxRunningNumb == 0) {
		$('#ajax_loadImg').html('');
	}
}

// URI/URL Encoder
function encodeMyURI(s){
    s=encodeURIComponent(s).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28");
   return s.replace(/\)/g, "%29").replace(/\+/g, "%2B").replace(/\#/g, "%23").replace(/\^/g, "%5E");
}


/// TRIM string ///
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, "");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/, "");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/, "");
}
