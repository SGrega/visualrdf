/*
 *	selectable Endpoints
 * 
 * Created by GregaS.
 */


var endpointTab = new Object();

//ajpesDB
endpointTab['local'] = {};// OBJECT! 
endpointTab['local'].url="http://localhost:3030/ds/query";
endpointTab['local'].needAuthorization=false;
endpointTab['local'].resource="http://www.lavbic.net/owl/AJPES.owl#";

//dbpediaDB
endpointTab['dbpedia'] = {};
endpointTab['dbpedia'].url="http://dbpedia.org/sparql";
endpointTab['dbpedia'].needAuthorization=false;

//lavbic-opendataDB
endpointTab['lavbicNews'] = {};
endpointTab['lavbicNews'].url="http://opendata.lavbic.net/news/sparql";
endpointTab['lavbicNews'].needAuthorization=true;
endpointTab['lavbicNews'].authorization = make_base_auth("student", "student"); //base64

//lodDB - experimantal, not tested
endpointTab['bbc'] = {};
endpointTab['bbc'].url="http://lod.openlinksw.com/sparql/";
endpointTab['bbc'].needAuthorization=false;


//AUTORIZATION hash
function make_base_auth(user, password) {
	    		  var tok = user + ':' + password;
	    		  var hash = btoa(tok);
	    		  return "Basic " + hash;
}
